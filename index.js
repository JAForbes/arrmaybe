export const Just = x => [x]
export const Nothing = () => [] 
export function flatMapPolyfill(f, bindArg){
    Array.prototype.flatMap = 
    Array.prototype.flatMap || this.reduce(
        (p,n,i,list) => 
            p.concat( f.call(bindArg || this, n, i, list) )
        ,this
    )
}

export const any = xs => 
    Just( xs.flatMap( x => x ) )
        .filter( xs => xs.length > 0 )

export const all = xs => 
    any(xs).filter(
        ys => ys.length === xs.length
    )

export const map = f => M => M.map( x => f(x) )
export const flatMap = f => M => M.flatMap( x => f(x) )
export const filter = f => M => M.filter( x => f(x) )
export const concat = (a,b) => 
    all([a,b]).map(
        ([a,b]) => a.concat(b)
    )
export const alt = (a,b) =>
    [a,b].filter(isJust).slice(0,1)

export const justs = Ms => Ms.flatMap( x => x )
export const isJust = M => M.length > 0
export const isNothing = M => M.length == 0
export const bifoldType = ({ bifold }) =>
    bifold(Nothing(), Just)

export const safe = f => x => {
    try {
        return Just(f(x))
    } catch {
        return Nothing()
    }
}