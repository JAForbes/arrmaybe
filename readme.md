ArrMaybe
========

Doing FP in Javascript is make believe.  This library helps us pretend we have a Maybe type in Native JS by piggy backing on the built in Array type.

The library is almost entirely static methods except for the included `flatMapPolyfill`.

There is some existing and planned interop with [static-sum-type](https://gitlab.com/JAForbes/static-sum-type).

The documentation / tests are very light / nonexistent because it's easier to just read the source and see what's going on than to document it.

### Why?

I find myself writing these helpers all the time, may as well publish/reuse/share them.

#### Quick Start

`npm install arrmaybe`

[Live Demo](https://flems.io/#0=N4IgZglgNgpgziAXAbVAOwIYFsZJAOgCsEAaEAYwHs0AXGWvAQQCdmBZDATwCMZ8woGGhwAOABUpROkKFAAUASgA6aFVTRwaAAhEZmcGACkAygHkAcloC8WlVvtaW7Lr3xwMYGHJMX8u-TDKqmjqmloA5jDaNgDW1gB8WpQJtmgOjqwcPHyGAK6acpQK-ILCGCJyqenpyVaJcRBpyXbV9gD8Gc7Z+HkFlMgxALoKVa1aiJ1ZruaUNAAWjeGKo-ZBatRhYNZacAkt9v4GPuZycEHpJUKilZE0lQDkAB73WiNraBVgDwA8j1hQ8QAjN8APR-AEvBRBEEglLIQYqFSfB7AAC+kOhsLqWnhiI+ci+WnuwFsIG4SiQWkBWnRr0xcIRwWRRJJFMeFIm1NpUJUMLhgMGIDIBlg5BoEA2eAATABWRBSgC0ssQgIAnCBUYMyFBGjEECh0NhcIgQHpmFgXLgyLlmFA8HMaDQRHBEDDch8YuF8FQsCCzRbsgABAAM+Cl+EBIIAJhBNH7WAHXLksFGiKQQDROCJjSA4ORmBARDQNYNUUA)

```js
import * as ArrMaybe from 'arrmaybe'

ArrMaybe.flatMapPolyfill()

const parseJSON = 
    ArrMaybe.safe(JSON.parse)

const get = k => o => 
    ArrMaybe.Just(o).flatMap( 
        o => k in o 
        ? ArrMaybe.Just(o[k]) 
        : ArrMaybe.Nothing() 
    )

const f = s =>
    parseJSON(s)
    .flatMap( get( 'x' ) )

f( '<xml>1</xml>' )
// => []

f( '{}' )
// => []

f( '{ "b": 1 }' )
// => []

f( '{ "x": 1 }' )
// => [1]
```


#### Static API

`Just`

Wrap a value in a list of one value. This represents what is traditionally called `Just` or `Some` in languages like Haskell and Scala.

`Nothing`

Returns an empty list.  This represents what is traditionally called `Nothing` or `None`.

`flatMapPolyfill`

Mutates Array prototype to have `flatMap`.  A method which is already supported in many modern browsers.

`any`

Accepts a list of `Maybe`'s, and returns a `Maybe` of a list of values that were not `Nothing`.

`all`

Accepts a list of `Maybe`'s and returns a `Maybe` of a list of values if there were no `Nothing` values in the list.

Otherwise returns `Nothing`.

`map`

A static curried version of `Array.prototype.map`, that doesn't pass in the Array index.

`flatMap`

A static curried version of `Array.prototype.flatMap` that doesn't pass in the Array index.

`filter`

A static curried version of `Array.prototype.filter` that doesn't pass in the Array index.

`concat`

Accepts two `Maybe` values, if both are `Just` it will execute the `concat` method on the value inside both Maybe's and wrap the result in a `Just`, otherwise `Nothing`.

`alt`

Accepts two `Maybe` values, if both are `Just` it will return the first.  If only the second is `Just` it will return the second.  Otherwise `Nothing`.

`justs`

Accepts a list of Maybe's and returns a list of the values that were contained in any `Just`'s within the original list.

`isJust`

Accepts a `Maybe`, returns `true` if the `Maybe` is `Just`

`isNothing`

Accepts a `Maybe`, returns `true` if the `Maybe` is `Nothing`

`bifoldType`

Accepts an object with a bifold function.  Populates the `bifold` with
`Nothing` if the left function is called, or `Just` if the right function is called.  Waits for the bifold value to fully apply the function.

Meant to be used with [static-sum-type](https://gitlab.com/JAForbes/static-sum-type)

```js
import * as sst from 'static-sum-type'
import * as Arr from 'arrmaybe'

const Loaded = sst.either('Loaded')

const notLoaded = Loaded.N(55)
const loaded = Loaded.Y('Hi there')

const f = x => x
    Arr.Just(x)
    .flatMap(
        bifoldType(Loaded)
    )
    .concat('Not loaded yet')
    .slice(0,1)
    .forEach(console.log)

f(notLoaded) // logs: 'Not loaded yet'
f(loaded) // logs: 'Hi There'
```

`safe`

Accepts a function, and then a value.  Runs the value against the function.  If it throws an exception returns `Nothing`, if it does not, it returns a `Just` of the return value of the original function.

