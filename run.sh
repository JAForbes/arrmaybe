#!/bin/bash

# See:
# http://james-forbes.com/#!/posts/alternative-to-npm-scripts
# Add npm binaries to path
export PATH=./node_modules/.bin:$PATH
set -e

function build-umd(){
    $(npm bin)/rollup -i index.js -f umd -n ArrMaybe -o dist/arrmaybe.umd.js -m dist/arrmaybe.umd.js.map
}

function preversion(){
    build-umd
}

eval $@